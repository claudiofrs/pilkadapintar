
  //var develPath = "assets/dev/";
  module.exports = function(grunt) {
    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),

      //  JSHINT - Checking js error
      jshint: {
        myFiles: [
          'js/surabaya.js',
          'js/indonesia.js',
          'js/faq.js',
          'js/statistik.js',
          'js/general.js'
        ]
      },

      // UGLIFY - Uglify js script
      uglify: {
        my_target: {
          files: {
            'js/script.js': ['js/general.js'],
            'js/sby.js': ['js/surabaya.js'],
            'js/ina.js': ['js/indonesia.js'],
            'js/fq.js': ['js/faq.js'],
            'js/stts.js': ['js/statistik.js'],
          }
        }
      },

      // SASS COMPILE - Compile sass
      sass: {                              
        dist: {                            
          options: {                       
            style: 'nested',
            noCache: true,
            quiet: false
          },
          files: {                         
            'css/style.css': 'css/scss/style.scss'
          }
        }
      },

      // WATCH - Watch modified file
      watch: {
        scripts: {
          files: [  'js/surabaya.js', 'js/indonesia.js', 'js/faq.js', 'js/statistik.js', 'js/general.js', ],
          tasks: [  'jshint', 
                    'uglify'],
          options: {
            spawn: false,
          },
        },
        stylesheet: {
          files: 'css/scss/**/*.scss',
          tasks: ['sass'],
          options: {
            spawn: false,
            reload: true
          },
        },
        /*php: {
          files: '../application/views/*.php',
          options: {
            spawn: false,
            reload: true
          }
        }*/
      },

    });
  
    grunt.loadNpmTasks('grunt-contrib-sass');   // Sass compiling
    grunt.loadNpmTasks('grunt-contrib-jshint'); // jshint for watch
    grunt.loadNpmTasks('grunt-contrib-uglify'); // load the given tasks
    grunt.loadNpmTasks('grunt-contrib-watch'); // watch grunt

    grunt.registerTask('default', ['watch']); // Default grunt task 

  };