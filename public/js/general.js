/* 
*  Javascript
*  Claudio Fresta
*  http://www.yodio.id
*  2015
*/

$('document').ready(function(){
$('[data-toggle="tooltip"]').tooltip(); 
$('[data-toggle="popover"]').popover(); 

$(function(){

	function stripTrailingSlash(str) {
		if(str.substr(-1) == '/') {
				return str.substr(0, str.length - 1);
			}
			return str;
		}

		var url = window.location.pathname;  
		//var base_url = 'http://localhost';
		//var host = "http://" + window.location.hostname + devel;
		var host = "http://" + window.location.hostname;
		var activePage = host.concat(url);

		$('.nav > li > a').each(function() {  
			var currentPage = $(this).attr('href');
			if (activePage == currentPage) {
				$(this).parent().addClass('active'); 
			}
		});

	}); 


	$(window).scroll(function(e){
		var scroll = $(window).scrollTop();
		if(scroll > 100){
			$('.arrow-totop').fadeIn('slow');
		}else{
			$('.arrow-totop').fadeOut('fast');
		}
	});

	$("a[href='#top']").click(function() {
		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});
	
	//console.log = function(){};
	
});