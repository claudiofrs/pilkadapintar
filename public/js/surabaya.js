
/* 
*  Javascript - Pilkada Surabata
*  Claudio Fresta
*  http://www.yodio.id
*  2015
*/

$('document').ready(function(){

/*	$('.content-inner#content-rasiyo .berita-terkait #rasiyo').endlessScroll({
		inflowPixels: 100,
		content: function(i, d, p){

			return 'hahaha';

		}
	});*/

	var key = '1620e438fbdea9561684d8d6879f3f92';
	var devel = "/mainan/ci-pilkadapintar";
	//var host = "http://" + window.location.hostname + devel;
	var host = "http://" + window.location.hostname;
	
	(function($, undefined){

		String.prototype.toProperCase = function () {
			return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
		};

	})(jQuery);

	function chunkGelar(nama){
		return nama.split(/((\.\s)|(\,\s))/g);
	}

	function toTitleCase(str){
		return str.charAt(0).toUpperCase() + str.slice(1);
	}	

	function domListArray(uli){
		var size = uli.length;
		var str = '<ul>';
		for (var i = 0; i <= size-1; i++) {
			str += '<li>'+uli[i]+'</li>';
		}
		str += '</ul>';
		return str;
	}

	function domContentSurabaya(data, calon, content){

		if(calon == 'rasiyo'){
			
			if(content == 'info'){

				// info rasiyo
				// 
				var gelar = [];
				gelar[0] = chunkGelar(data[0].name);
				gelar[1] = chunkGelar(data[1].name);

				var pekerjaan = [];
				pekerjaan[0] = data[0].work;
				pekerjaan[1] = data[1].work;
				if(!pekerjaan[0]){
					pekerjaan[0] = "-";
				}else if(!pekerjaan[1]){
					pekerjaan[1] = "-";
				}

				var isIncumbent;
				if(data.incumbent === "0"){
					isIncumbent = "Calon Baru";
				}else{
					isIncumbent = "Calon Incumbent";
				}

				$('.content-inner#content-rasiyo').empty();
				$('.content-inner#content-rasiyo').empty();
				var strRasiyo = '<div class="bio"><div class="col-xs-6" id="rasiyo"><br><img src="'+host+'/public/img/ava-rasiyo.png" alt="rasiyo"><ul class="info-calon">'+
				'<li><h5>'+gelar[0][4]+'</h5><p>'+data[0].kind+' Walikota</p></li>' +
				'<li><h5>Partai Amanat Nasional</h5><p>Partai Pengusung</p></li>' +
				'<li><h5>'+gelar[0][0]+', '+gelar[0][8]+'</h5><p>Gelar</p></li>' +
				'<li><h5>'+pekerjaan[0]+'</h5><p>Pekerjaan Saat Ini</p></li>'+
				'<li><h5>'+isIncumbent+'</h5><p>Status Calon</p></li></ul></div>';

				var strLucy = '<div class="col-xs-6" id="lucy"><br><img src="'+host+'/public/img/ava-lucy.png" alt="lucy"><ul class="info-calon">'+
				'<li><h5>'+gelar[1][4]+'</h5><p>Calon Wakil Walikota</p></li>'+
				'<li><h5>Partai Demokrat</h5><p>Kader Partai</p></li>'+
				'<li><h5>'+gelar[1][0]+'</h5><p>Gelar</p></li>'+
				'<li><h5>'+pekerjaan[1]+'</h5><p>Pekerjaan saat ini</p></li>'+
				'<li><h5>'+isIncumbent+'</h5><p>Status Calon</p></li></ul></div></div>';

				$('.content-inner#content-rasiyo').append(strRasiyo);
				$('.content-inner#content-rasiyo').append(strLucy);
			}
			else if(content == 'visi'){

				$('.content-inner#content-rasiyo').empty();
				var strVisi = '<div class="visi"><div class="col-xs-12" id="rasiyo"><h3>'+toTitleCase(data.toLowerCase())+'</h3></div></div>';
				$('.content-inner#content-rasiyo').append(strVisi);

			}
			else if(content == 'misi'){

				var list = JSON.stringify(data);
				list = list.split('\\n');
				console.log(list);
				list = domListArray(list);
				$('.content-inner#content-rasiyo').empty();
				var strMisi = '<div class="misi"><div class="col-xs-12" id="rasiyo">'+list+'</div></div>';
				$('.content-inner#content-rasiyo').append(strMisi);

			}

		}else{
			if(content == 'info'){

				// info rasiyo
				// 
				var gelarRisma = [];
				gelarRisma[0] = chunkGelar(data[0].name);
				gelarRisma[1] = chunkGelar(data[1].name);

				var pekerjaanRisma = [];
				pekerjaanRisma[0] = data[0].work;
				pekerjaanRisma[1] = data[1].work;
				if(!pekerjaanRisma[0]){
					pekerjaanRisma[0] = "-";
				}else if(!pekerjaanRisma[1]){
					pekerjaanRisma[1] = "-";
				}

				var isIncumbentRisma;
				if(data.incumbentRisma === "0"){
					isIncumbentRisma = "Calon Baru";
				}else{
					isIncumbentRisma = "Calon Incumbent";
				}

				$('.content-inner#content-risma').empty();
				$('.content-inner#content-risma').empty();
				var strRisma = '<div class="bio"><div class="col-xs-6" id="risma"><br><img src="'+host+'/public/img/ava-risma.png" alt="risma"><ul class="info-calon">'+
				'<li><h5>'+gelarRisma[0][4]+'</h5><p>'+data[0].kind+' Walikota</p></li>' +
				'<li><h5>Partai Demokrasi Indonesia </h5><p>Partai Pengusung</p></li>' +
				'<li><h5>'+gelarRisma[0][0]+', '+gelarRisma[0][8]+'</h5><p>Gelar</p></li>' +
				'<li><h5>'+pekerjaanRisma[0]+'</h5><p>Pekerjaan Saat Ini</p></li>'+
				'<li><h5>'+isIncumbentRisma+'</h5><p>Status Calon</p></li></ul></div>';

				var strWhisnu = '<div class="col-xs-6" id="risma"><br><img src="'+host+'/public/img/ava-wisnu.png" alt="wisnu"><ul class="info-calon">'+
				'<li><h5>'+gelarRisma[1][0]+'</h5><p>Calon Wakil Walikota</p></li>'+
				'<li><h5>Partai Demokrasi Indonesia </h5><p>Kader Partai</p></li>'+
				'<li><h5>'+gelarRisma[1][4]+'</h5><p>Gelar</p></li>'+
				'<li><h5>'+pekerjaanRisma[1]+'</h5><p>Pekerjaan saat ini</p></li>'+
				'<li><h5>'+isIncumbentRisma+'</h5><p>Status Calon</p></li></ul></div></div>';

				$('.content-inner#content-risma').append(strRisma);
				$('.content-inner#content-risma').append(strWhisnu);
			}
			else if(content == 'visi'){

				$('.content-inner#content-risma').empty();
				var strVisiRisma = '<div class="visi"><div class="col-xs-12" id="risma"><h3>'+toTitleCase(data.toLowerCase())+'</h3></div></div>';
				$('.content-inner#content-risma').append(strVisiRisma);

			}
			else if(content == 'misi'){

				var listRisma = JSON.stringify(data);
				listRisma = listRisma.split('\\n');
				console.log(listRisma);
				listRisma = domListArray(listRisma);
				$('.content-inner#content-risma').empty();
				var strMisiRisma = '<div class="misi"><div class="col-xs-12" id="risma">'+listRisma+'</div></div>';
				$('.content-inner#content-risma').append(strMisiRisma);

			}
		}

	}

	function modContent(calon, content){
		if(calon == 'rasiyo'){
			id = 1240;
			if(content == 'info'){
				$.ajax({
					type: "GET",
					dataType: "JSON",
					url: "http://api.pemiluapi.org/candidate-pilkada-surabaya/api/candidates?id="+id+"&apiKey="+key,
					success: function(data){
						var contentRasiyo = [];
						console.log(data);
						var response = JSON.parse(JSON.stringify(data));
						contentRasiyo[0] = response.data.results.candidates['0'].participants['0'];
						contentRasiyo[1] = response.data.results.candidates['0'].participants['1'];
						contentRasiyo.incumbent = response.data.results.candidates['0'].pertahana;
						domContentSurabaya(contentRasiyo, calon, content);
					}
				});
			}else if((content == 'visi') || (content == 'misi')){
				$.ajax({
					type: "GET",
					dataType: "JSON",
					url: "http://api.pemiluapi.org/candidate-pilkada-surabaya/api/vision_missions?apiKey="+key+"&candidate_id="+id,
					success: function(data){
						localStorage.setItem("visimisiRasiyo", JSON.stringify(data));
					}
				});
				lsvisimisiRasiyo = localStorage.getItem("visimisiRasiyo");
				parsedLsVisimisiRasiyo = JSON.parse(lsvisimisiRasiyo);
				if(content == 'visi'){
					var visi = parsedLsVisimisiRasiyo.data.results.vision_missions[0].vision;
					domContentSurabaya(visi, calon, content);
				}
				else if(content == 'misi'){
					var misi = parsedLsVisimisiRasiyo.data.results.vision_missions[0].mision;	
					console.log(misi);
					domContentSurabaya(misi, calon, content);
				}
			}

		}else if(calon == 'risma'){
			id = 253;

			if(content == 'info'){
				$.ajax({
					type: "GET",
					dataType: "JSON",
					url: "http://api.pemiluapi.org/candidate-pilkada-surabaya/api/candidates?id="+id+"&apiKey="+key,
					success: function(data){
						var contentRasiyo = [];
						console.log(data);
						var response = JSON.parse(JSON.stringify(data));
						contentRasiyo[0] = response.data.results.candidates['0'].participants['0'];
						contentRasiyo[1] = response.data.results.candidates['0'].participants['1'];
						contentRasiyo.incumbent = response.data.results.candidates['0'].pertahana;
						domContentSurabaya(contentRasiyo, calon, content);
					}
				});
			}else if((content == 'visi') || (content == 'misi')){
				$.ajax({
					type: "GET",
					dataType: "JSON",
					url: "http://api.pemiluapi.org/candidate-pilkada-surabaya/api/vision_missions?apiKey="+key+"&candidate_id="+id,
					success: function(data){
						localStorage.setItem("visimisiRisma", JSON.stringify(data));
					}
				});
				lsvisimisiRisma = localStorage.getItem("visimisiRisma");
				parsedLsVisimisiRisma = JSON.parse(lsvisimisiRisma);
				if(content == 'visi'){
					var visiRisma = parsedLsVisimisiRisma.data.results.vision_missions[0].vision;
					domContentSurabaya(visiRisma, calon, content);
				}
				else if(content == 'misi'){
					var misiRisma = parsedLsVisimisiRisma.data.results.vision_missions[0].mision;	
					console.log(misiRisma);
					domContentSurabaya(misiRisma, calon, content);
				}
			}
		}
	}

	function getMatchTitle(calon, data){

		var chunkedTitle = data.trim().split(/(\-|\ )/gmi);
		chunkedTitle = chunkedTitle.filter(function(v){return v!=="";});
		var status;
		
		if(calon == 'rasiyo'){
			for(var i = 0 ; i <= chunkedTitle.length - 1 ;i++ ){
				if((chunkedTitle[i].toLowerCase() == 'rasio') || (chunkedTitle[i].toLowerCase() == 'rasiyo') || (chunkedTitle[i].toLowerCase() == 'lucy')){
					status = 'ketemu';
					break;
				}
				status = 'nomatch';
			}
			return status;
		}else{
			for(var j = 0 ; j <= chunkedTitle.length - 1 ;j++ ){
				if((chunkedTitle[j].toLowerCase() == 'risma') || (chunkedTitle[j].toLowerCase() == 'wisnu') || (chunkedTitle[j].toLowerCase() == 'whisnu')){
					status = 'ketemu';
					break;
				}
				status = 'nomatch';
			}
			return status;
		}

	}

	function getMatchNews(calon, data){
			
		var berita = data.data.results.news;
		var holderNews = [];
		for (var i = 0; i <= berita.length - 1 ; i++) {

			if(getMatchTitle(calon, berita[i].title) == 'ketemu'){
				holderNews.push(berita[i]);
			}

		}

		console.log(holderNews.length);

		return holderNews;

	}

	function matchCalon(calon, isAppend){

		var news = localStorage.getItem('news');
		var parsedNews = JSON.parse(news);
		console.log(parsedNews);

		var selectedNews = getMatchNews(calon, parsedNews);
		localStorage.setItem('selectedNews',news);
		str = '<div class="berita-terkait"><div class="col-xs-12"><ul>';
		for (var i = 0; i <= selectedNews.length - 1; i++) {
			str += '<li><a href="'+selectedNews[i].link+'" target=_blank()><h5>';
			str += selectedNews[i].title+'</h5></a><p>'+selectedNews[i].resource.name+'</p></li>';
		}
		str += '</ul>';

		if(isAppend === 1){
			if(calon == 'rasiyo'){
				//rasiyo
				$('.content-inner#content-rasiyo').empty();
				$('.content-inner#content-rasiyo').append(str);
				$('.content-inner#content-rasiyo .berita-terkait div').slimScroll({
					height: '290px',
					alwaysVisible: true,
					color: '#E52B2A',
					size: '5px'
				});

			}else{
				// risma
				$('.content-inner#content-risma').empty();
				$('.content-inner#content-risma').append(str);
				$('.content-inner#content-risma .berita-terkait div').slimScroll({
					height: '290px',
					alwaysVisible: true,
					color: '#E52B2A',
					size: '5px'
				});
			}
		}

		return selectedNews.length;


	}

	function countPopularity(calon, allnews, selectedNews){
		
		if(calon == 'rasiyo'){
			console.log(selectedNews + ' / ' + allnews);
			pop = (selectedNews/allnews)*100;
			//alert(pop);
			$('.value-pop-media#rasiyo').empty();
			$('.kalkulasi-media#rasiyo').empty();
			$('.value-pop-media#rasiyo').append(pop.toFixed(1)+'%');
			$('.kalkulasi-media#rasiyo').append(selectedNews +' dari '+ allnews +' artikel terdapat berita mengenai calon');
		}else{
			console.log(selectedNews + ' / ' + allnews);
			pop = (selectedNews/allnews)*100;
			$('.value-pop-media#risma').empty();
			$('.kalkulasi-media#risma').empty();
			$('.value-pop-media#risma').append(pop.toFixed(1)+'%');
			$('.kalkulasi-media#risma').append(selectedNews +' dari '+ allnews +' artikel terdapat berita mengenai calon');
		}
	}

	function fetchNews(calon, isAppend){

		// offset: Pengambilan data dari data keberapa?
		// limit: Berapa banyak data yang difetch dari data ke-offset?
		if(localStorage.getItem('news')){
			console.log('ada data');
			var selectedNews = matchCalon(calon, isAppend);
			var news = localStorage.getItem('news');
			var parsedNews = JSON.parse(news);
			console.log(parsedNews);
			//console.log(news);
			var allnews = parsedNews.data.results.news.length;
			countPopularity(calon, allnews, selectedNews);
		}else{
			console.log('data kosong');
			$.ajax({
				type: "GET",
				dataType: "JSON",
				url: "http://api.pemiluapi.org/berita-pilkada-surabaya/api/news?apiKey="+key,
				success: function(data){
					localStorage.setItem('newsCount', JSON.stringify(data));
					var allnews = data.data.results.total;
					localStorage.setItem('newsCount', allnews);
				}
			});
			var newsCount = localStorage.getItem('newsCount');
			$.ajax({
				type: "GET",
				dataType: "JSON",
				url: "http://api.pemiluapi.org/berita-pilkada-surabaya/api/news?limit="+newsCount+"&apiKey="+key,
				success: function(data){
					localStorage.setItem('news', JSON.stringify(data));
					var selectedNews = matchCalon(calon, isAppend);
					var allnews = data.data.results.news.length;
					countPopularity(calon, allnews, selectedNews);
				}
			});
		}


	}

	$('#info-rasiyo').click(function(event){
		modContent('rasiyo', 'info');
		($(this).parent().siblings()).removeClass('active');
		$(this).parent().addClass('active');
		event.preventDefault();
	});
	$('#visi-rasiyo').click(function(event){
		modContent('rasiyo', 'visi');
		($(this).parent().siblings()).removeClass('active');
		$(this).parent().addClass('active');
		event.preventDefault();
	});
	$('#misi-rasiyo').click(function(event){
		modContent('rasiyo', 'misi');
		($(this).parent().siblings()).removeClass('active');
		$(this).parent().addClass('active');
		event.preventDefault();
	});
	$('#berita-rasiyo').click(function(event){
		fetchNews('rasiyo', 1);
		($(this).parent().siblings()).removeClass('active');
		$(this).parent().addClass('active');
		event.preventDefault();
	});

	$('#info-risma').click(function(event){
		modContent('risma', 'info');
		($(this).parent().siblings()).removeClass('active');
		$(this).parent().addClass('active');
		event.preventDefault();
	});
	$('#visi-risma').click(function(event){
		modContent('risma', 'visi');
		($(this).parent().siblings()).removeClass('active');
		$(this).parent().addClass('active');
		event.preventDefault();
	});
	$('#misi-risma').click(function(event){
		modContent('risma', 'misi');
		($(this).parent().siblings()).removeClass('active');
		$(this).parent().addClass('active');
		event.preventDefault();
	});
	$('#berita-risma').click(function(event){
		fetchNews('risma', 1);
		($(this).parent().siblings()).removeClass('active');
		$(this).parent().addClass('active');
		event.preventDefault();
	});


	// Init
	$('.thumbnail-left').addClass('animated fadeIn');
	$('.thumbnail-right').addClass('animated fadeIn');
	setTimeout(function(){
		localStorage.clear('news');
		fetchNews('rasiyo', 0);
		fetchNews('risma', 0);
		//var url = 'http://localhost/mainan/ci-pilkadapintar';
		var url = host;
		$('.detail-content > #content-rasiyo').append('<img src="'+url+'/public/img/loader.gif'+'" style="text-align: center; margin: 0 auto; display: block; width: 50px; margin-top: 100px;">');
		$('.detail-content > #content-risma').append('<img src="'+url+'/public/img/loader.gif'+'" style="text-align: center; margin: 0 auto; display: block; width: 50px; margin-top: 100px;">');
		setTimeout(function(){
			modContent('rasiyo', 'info'); }, 1500);
		setTimeout(function(){
			modContent('risma', 'info'); }, 1500);
		$('#info-rasiyo').parent().addClass('active');
		$('#info-risma').parent().addClass('active');
		//modContent('risma', 'info');
		//modContent('rasiyo', 'visi');
	}, 1000);


});