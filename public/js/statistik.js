/* 
*  Javascript
*  Claudio Fresta
*  http://www.yodio.id
*  2015
*/

$('document').ready(function(){
	
	var key = '1620e438fbdea9561684d8d6879f3f92';
	var devel = "/mainan/ci-pilkadapintar";
	var host = "http://" + window.location.hostname + devel;
	//var host = "http://" + window.location.hostname;

	// get by provinsinya pake limit offset, soalnya by year ga bisa
	// call record tahun pake limit offset juga per 33
	function callDataAll(limit, offset, callback){

		$.ajax({
			type: 'GET',
			url: 'http://api.pemiluapi.org/idiapi/api/idi?apiKey='+key+'&limit='+limit+'&offset='+offset,
			dataType: 'JSON',
			success: function(data){
				callback(data);
			}
		});

	}

	callProvinsi(key, function(data){

		var dataString = JSON.stringify(data);
		var strProvinsi = '';
		//localStorage.setItem('provinsi', dataString);
		for (var i = 0; i < data.data.results.count; i++) {
			strProvinsi += '<option value="'+i+'">'+data.data.results.provinces[i].nama+'</option>';
		}
		$(".select-provinsi-stats").append(strProvinsi);
		$(".select-provinsi-stats").select2();

	});

	function callProvinsi(key, callback){
		$.ajax({
			type: 'GET',
			dataType: 'JSON',
			url: 'http://api.pemiluapi.org/calonpilkada/api/provinces?apiKey='+key,
			success: function(data){
				callback(data);
			}
		});
	}

	function filterProvinsi(id){

		$.ajax({
			type: 'GET',
			url: 'http://api.pemiluapi.org/idiapi/api/idi?apiKey='+key+'&limit=1&offset='+id,
			dataType: 'JSON',
			success: function(data){
				localStorage.setItem('dataProv-'+id, JSON.stringify(data));
			}
		});

	}

	function filterIndikator(idProvinsi, idIndikator){

		var parsedDataYear;
		console.log(localStorage.getItem('dataProv-'+idProvinsi));
		for (var i = 0; i < 6; i++) {
			var dataProvIndikator = {};
			var dataYear = localStorage.getItem('dataProv-'+idProvinsi);
			parsedDataYear = JSON.parse(dataYear);
			var keyParsed = Object.keys(parsedDataYear.data.results.idi.indicator[0]);
			console.log(i);
			console.log(keyParsed);
			//dataProvIndikator.year = parsedDataYear.data.results.idi.year;
			//dataProvIndikator.indikator = keyParsed
			//localStorage.setItem('graphProv-'+idProvinsi,)
		}


	}

	//var glbArrYear = {};

	$('.select-provinsi-stats').change('select2-selecting', function(e){
		//alert($(this).val());
		// 5 from banyak tahun
		alert('haha');
		var glbArrYearContent = [];
		var offsetId = $(this).val();
		for (var i = 0; i < 6; i++) {
			// save to localstorage
			filterProvinsi(offsetId);
			offsetId = parseInt(offsetId) + 33; // banyak provinsi
			offsetId.toString();
		}
		filterIndikator(0, 1);
		
	});



});
