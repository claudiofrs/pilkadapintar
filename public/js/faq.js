$("document").ready(function(){

	var key = '1620e438fbdea9561684d8d6879f3f92';
	var devel = "/mainan/ci-pilkadapintar";
	//var host = "http://" + window.location.hostname + devel;
	var host = "http://" + window.location.hostname;

	//if(loadQuestion('')){
		popQuestion(4);
	//}
	var faqData = localStorage.getItem('faq');
	var parsedFaqData = JSON.parse(faqData);
	console.log(parsedFaqData);

	var questions = [];
	for (var i = 0; i < parsedFaqData.data.results.count; i++) {
		questions.push(parsedFaqData.data.results.faqs[i].question_text);
	}

	function uiSearchJob(str, answer, hukum, longAnswer){
		console.log(str, answer, hukum, longAnswer);
		var appStr = '';
		$('.content-result-faq').empty();
		$('.title-search > .title-search-content').fadeOut();
		$('.typeahead-faq-outer').addClass('totop');
		$('.content-result-faq').show();
		setTimeout(function(){
			var url = host;
			$('.content-result-faq').append('<img src="'+url+'/public/img/loader.gif'+'" style="text-align: center; margin: 0 auto; display: block; width: 50px; margin-top: 30px; margin-bottom: 40px;" class="loader-faq">');
		}, 1000);
		setTimeout(function(){
			$('.loader-faq').remove();
			appStr = '<h3 style="margin-top: 10px;">'+str+'</h3><hr><p style="font-size: 13px;">'+answer+'</p><p style="opacity: .7;">'+longAnswer+'</p><p style="opacity: .3; margin-bottom: 30px;">Sumber: '+hukum+'</p>';
			$('.content-result-faq').append(appStr);
		}, 1500);
			
		
	}

	function gotoQuestion(str){
		for (var i = 0; i < parsedFaqData.data.results.count; i++) {
			if(parsedFaqData.data.results.faqs[i].question_text == str){
				uiSearchJob(str, 
							parsedFaqData.data.results.faqs[i].question_answer, 
							parsedFaqData.data.results.faqs[i].relevant_laws_regulations, 
							parsedFaqData.data.results.faqs[i].relevant_text);
			}
		}
	}

	function popQuestion(limit){

		parsedFaqData = JSON.parse(localStorage.getItem('faq'));

		for (var i = 0; i < 4; i++) {
			var question = parsedFaqData.data.results.faqs[i].question_text;
			var answer = parsedFaqData.data.results.faqs[i].question_answer;
			var law = parsedFaqData.data.results.faqs[i].relevant_laws_regulations;
			var desc = parsedFaqData.data.results.faqs[i].relevant_text;
			var tag = parsedFaqData.data.results.faqs[i].tags;
			$('.faq-populer').append('<div class="col-xs-6"><div class="card card-faq"><a href="#" class="pop-question"><h5>'+question+'</h5></a><span class="label label-default faq-populer-tag">'+tag+'</span></div></div>');
		}
		
	}

	$(document).on('click', '.pop-question' , function(e) {
		var questionPop = $(this).find('h5').text();
		gotoQuestion(questionPop);
	});

	function loadQuestion(limit){
		callAllQuestion(limit, function(data){
			localStorage.setItem('faq', JSON.stringify(data));
		});
	}

	function callAllQuestion(limit, callback){
		if(!limit) limit = 95;
		$.ajax({
			type: 'GET',
			dataType: 'JSON',
			url: 'http://api.pemiluapi.org/faqpilkada/api/faqs?apiKey='+key+'&limit='+limit,
			success: function(data){
				callback(data);
			}
		});
	}

	var substringMatcher = function(strs) {
		return function findMatches(q, cb) {
			var matches, substringRegex;

		    // an array that will be populated with substring matches
		    matches = [];

		    // regex used to determine if a string contains the substring `q`
		    substrRegex = new RegExp(q, 'i');

		    // iterate through the pool of strings and for any string that
		    // contains the substring `q`, add it to the `matches` array
		    $.each(strs, function(i, str) {
		    	if (substrRegex.test(str)) {
		    		matches.push(str);
		    	}
		    });

		    cb(matches);
		};
	};

	$('#typeahead-faq').typeahead({
			hint: true,
			highlight: true,
			minLength: 1
		},
		{
			name: 'faq',
			source: substringMatcher(questions),
			limit: 10
	});	

	$('#typeahead-faq').bind('typeahead:select', function(ev, suggestion) {
		gotoQuestion(suggestion);
	});


});