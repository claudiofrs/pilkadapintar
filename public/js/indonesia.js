/* 
*  Javascript
*  Claudio Fresta
*  http://www.yodio.id
*  2015
*/

$('document').ready(function(){

/*	var data = [{ id: 0, text: 'enhancement' }, { id: 1, text: 'bug' }, { id: 2, text: 'duplicate' }, { id: 3, text: 'invalid' }, { id: 4, text: 'wontfix' }];*/
	
	var devel = "/mainan/ci-pilkadapintar";
	//var host = "http://" + window.location.hostname + devel;
	var host = "http://" + window.location.hostname;

	function toTitleCase(str){
		return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	}	

	function toSentenceCase(str){
		return str.charAt(0).toUpperCase() + str.slice(1);
	}	

	function chunkPartai(nama){
		return nama.split(/(\,\s)/g);
	}

	var key = '1620e438fbdea9561684d8d6879f3f92';

	callProvinsi(key, function(data){
		var dataString = JSON.stringify(data);
		var strProvinsi = '';
		strProvinsi += '<option value="all">Semua</option>';
		//localStorage.setItem('provinsi', dataString);
		for (var i = 0; i < data.data.results.count; i++) {
			strProvinsi += '<option value="'+data.data.results.provinces[i].id+'">'+data.data.results.provinces[i].nama+'</option>';
		}
		//console.log(strProvinsi);
		$(".select-provinsi").append(strProvinsi);
		$(".select-provinsi").select2();

	});

	$('.select-provinsi').change('select2-selecting', function(e){
		$('#right-content-card-caleg').empty();
		//var url = host;
		var url = host;
		$('#right-content-card-caleg').append('<img src="'+url+'/public/img/loader.gif'+'" style="text-align: center; margin: 0 auto; display: block; width: 50px; margin-top: 30px;">');
		populateRegionLoadProvinsiSelect2(key, $(this).val());
	});

	$('.select-daerah').change('select2-selecting', function(e){
		$('#right-content-card-caleg').empty();
		var url = host;
		if($(this).val() == 'all'){
			$('#right-content-card-caleg').append('<img src="'+url+'/public/img/loader.gif'+'" style="text-align: center; margin: 0 auto; display: block; width: 50px; margin-top: 30px;">');
			populateRegionLoadProvinsiSelect2(key, $('.select-provinsi').select2('val'));	
		}else{
			populateProvinsiLoadRegionSelect2(key, $('.select-provinsi').val(), $(this).val());
			
		}
	});

	populateRegionLoadProvinsiSelect2(key, null);

	function populateProvinsiLoadRegionSelect2(key, idProvinsi, idRegion){
		$('#right-content-card-caleg').empty();
		loadCalegRegion(key, idProvinsi, idRegion, 7, 0, 1);
	}

	function populateRegionLoadProvinsiSelect2(key, idProvinsi){
		callRegion(key, idProvinsi, function(data, idProvinsi){

			var strDaerah = '';
			var initProvinsi = idProvinsi;

			if(initProvinsi == 'all' || !idProvinsi){ // semua tanpa provinsi
				$('#right-content-card-caleg').empty();
				$(".select-daerah").select2('destroy');
				$(".select-daerah").empty();
				$(".select-daerah").append('<option value="all" selected="selected">Semua</option>');
				$(".select-daerah").select2();
				$(".select-daerah").attr('disabled', 'disabled');
				loadAllCaleg(key, 5, 0);
			}else{
				if(idProvinsi != 'all'){ // ada query provinsi
					$('#right-content-card-caleg').empty();
					$('#right-content-card-caleg').append('<img src="'+url+'/public/img/loader.gif'+'" style="text-align: center; margin: 0 auto; display: block; width: 50px; margin-top: 30px;">');
					loadCalegProvinsi(key, idProvinsi, 5, 0, 0, function(populated){
						if(!populated){
							$('#right-content-card-caleg').empty();
							$('#right-content-card-caleg').append('Tak Ada Calon');
							$(".select-daerah").select2('destroy');
							$(".select-daerah").empty();
							$(".select-daerah").append('<option value="all" selected="selected">Semua</option>');
							$(".select-daerah").select2();
							$(".select-daerah").attr('disabled', 'disabled');
						}else{

							$('#right-content-card-caleg').empty();
							$('#right-content-card-caleg').append(populated);

							$(".select-daerah").select2('destroy');
							$(".select-daerah").removeAttr('disabled');
							$(".select-daerah").empty();

							strDaerah += '<option value="all" selected="selected">Semua</option>';
							for (var i = 0; i < data.data.results.count; i++) {
								if(data.data.results.regions[i].provinsi.id == idProvinsi){
									strDaerah += '<option value="'+data.data.results.regions[i].id+'">'+data.data.results.regions[i].nama+'</option>';
								}
							}

							$(".select-daerah").append(strDaerah);
							$(".select-daerah").select2();

						}
					});
				}
			}
		});
	}

	$('.select-partai').select2();
	$('.select-gender-calon').select2();

	var provinsi = localStorage.getItem('provinsi');

	function breakPartai(namaPartai){
		console.log('breakPartai');

		if(!localStorage.getItem('partai')){
			callPartai();
		}

		var arrPartai = chunkPartai(namaPartai);
		var stringPartai = localStorage.getItem('partai');
		var parsedPartai = JSON.parse(stringPartai);
		var allDataPartai = [];
		for (var i = 0; i < parsedPartai.data.results.count; i++) {
			for (var j = 0; j < arrPartai.length; j++) {
				if(arrPartai[j] == parsedPartai.data.results.parties[i].nama){
					var dataPartai = [];
					dataPartai.logo = parsedPartai.data.results.parties[i].url_logo_small;
					dataPartai.web = parsedPartai.data.results.parties[i].url_situs;
					console.log('ketemu');
				}
			}
		}
		console.log(allDataPartai);
		return allDataPartai;
	}

	function callPartai(){
		$.ajax({
			type: 'GET',
			dataType: 'JSON',
			url: 'http://api.pemiluapi.org/partai/api/parties?apiKey='+key,
			success:function(data){
				localStorage.setItem('partai', JSON.stringify(data));
			}
		});
	}

	function printDukungan(dukungan){
		var strDukungan = '';
		for (var i = 0; i < dukungan.length; i++) {
			strDukungan += '<img src="'+dukungan[i].dataPartai.logo+'"></img>';
		}
		return strDukungan;
	}

	function calculateUsia(dob){
		var currentTime = new Date();
		var year = dob.split("-");
		var tahunIni = currentTime.getFullYear();
		return tahunIni - year[0];
	}

	function normalCaseTitle(str){
		if(str){
			return toTitleCase(str.toLowerCase());
		}else{
			return '-';
		}
	}

	function populateCaleg(data, isPrint){

				var strCardCaleg = '';

				for (i = 0; i < data.data.results.candidates.length; i++) {

					var candidateId = data.data.results.candidates[i].id_peserta;

					// LABEL LOKASI
					var namaProvinsi = toTitleCase((data.data.results.candidates[i].provinsi.nama).toLowerCase());
					var namaDaerah = toTitleCase((data.data.results.candidates[i].daerah.nama).toLowerCase());

					// INFO CALON
					var namaCalon = normalCaseTitle(data.data.results.candidates[i].paslon[0].nama);
					var kerjaCalon = normalCaseTitle(data.data.results.candidates[i].paslon[0].pekerjaan);
					var alamatCalon = normalCaseTitle(data.data.results.candidates[i].paslon[0].alamat);
					var daerahLahirCalon = normalCaseTitle(data.data.results.candidates[i].paslon[0].pob);
					if(!(data.data.results.candidates[i].paslon[0].dob) || isNaN(data.data.results.candidates[i].paslon[0].dob)){
						usiaCalon = '-';
					}else{
						usiaCalon = calculateUsia(data.data.results.candidates[i].paslon[0].dob)+' Tahun';
					}
					var jkCalon = data.data.results.candidates[i].paslon[0].jk;

					var namaWakil = normalCaseTitle(data.data.results.candidates[i].paslon[1].nama);
					var kerjaWakil = normalCaseTitle(data.data.results.candidates[i].paslon[1].pekerjaan);
					var alamatWakil = normalCaseTitle(data.data.results.candidates[i].paslon[1].alamat);
					var daerahLahirWakil = normalCaseTitle(data.data.results.candidates[i].paslon[1].pob);
					if(!(data.data.results.candidates[i].paslon[1].dob) || isNaN(data.data.results.candidates[i].paslon[1].dob)){
						usiaWakil = '-';
					}else{
						usiaWakil = calculateUsia(data.data.results.candidates[i].paslon[1].dob)+' Tahun';
					}
					var jkWakil = data.data.results.candidates[i].paslon[1].jk;

					var dukungan = data.data.results.candidates[i].dukungan;
					var statusCalon = data.data.results.candidates[i].incumbent;
					var sumberData = data.data.results.candidates[i].sumber;

					var statistik = {};
					var berkas = {};

					// DUKUNGAN
					statistik.jenisDukungan = normalCaseTitle(data.data.results.candidates[i].jenis_dukungan);
						if(!statistik.jenisDukungan) statistik.jenisDukungan = '-';
					statistik.jumlahDukunganAwal = data.data.results.candidates[i].jumlah_dukungan_awal;
						if(!statistik.jumlahDukunganAwal) statistik.jumlahDukunganAwal = '-';
					statistik.jumlahDukunganPenetapan = data.data.results.candidates[i].jumlah_dukungan_penetapan;
						if(!statistik.jumlahDukunganPenetapan) statistik.jumlahDukunganPenetapan = '-';
					statistik.jumlahDukunganPerbaikan = data.data.results.candidates[i].jumlah_dukungan_perbaikan;
						if(!statistik.jumlahDukunganPerbaikan) statistik.jumlahDukunganPerbaikan = '-';
					
					// BERKAS
					berkas.kelengkapan = normalCaseTitle(data.data.results.candidates[i].kelengkapan_dokumen);
						if(!berkas.kelengkapan) berkas.kelengkapan = '-';
					berkas.pemenuhanSyaratDukungan = normalCaseTitle(data.data.results.candidates[i].pemenuhan_syarat_dukungan);
						if(!berkas.pemenuhanSyaratDukungan) berkas.pemenuhanSyaratDukungan = '-';
					berkas.pemenuhanSyaratDukunganPerbaikan = normalCaseTitle(data.data.results.candidates[i].pemenuhan_syarat_dukungan_perbaikan);
						if(!berkas.pemenuhanSyaratDukunganPerbaikan) berkas.pemenuhanSyaratDukunganPerbaikan = '-';
					berkas.penerimaanDokumenPerbaikan = normalCaseTitle(data.data.results.candidates[i].penerimaan_dokumen_perbaikan);
						if(!berkas.penerimaanDokumenPerbaikan) berkas.kelengkapan = '-';
					berkas.statusPenerimaan = normalCaseTitle(data.data.results.candidates[i].status_penerimaan);
						if(!berkas.statusPenerimaan) berkas.statusPenerimaan = '-';


					if(!statusCalon){
						statusCalon = 'Calon Baru';
					}else{
						statusCalon = 'Calon Incumbent';
					}

					if(!dukungan){
						dukungan = '-';
					}

					if(jkCalon == 'P'){
						boolJKCalon = 1;
					}else{
						boolJKCalon = 0;
					}

					if(jkWakil == 'P'){
						boolJKWakil = 1;
					}else{
						boolJKWakil = 0;
					}

					var labelGender = '';
					//console.log(candidateId +' : ' jkCalon + 'vs' + jkWakil);
					if((boolJKCalon || boolJKWakil) == 1) labelGender = '<span class="label label-danger pull-right" style="background: transparent;color: #DC408F;border: solid 1px #DC408F; margin-top: 3px;">Calon Perempuan</span>';

					strCardCaleg += '<div class="card card-caleg" candidate-id="'+candidateId+'"><div class="row caleg-main-info"><div class="col-xs-12"><h5 class="content-caleg content-caleg-label"><span class="label label-danger">'+namaProvinsi+'</span><span class="label label-default">'+namaDaerah+'</span>'+labelGender+'</h5></div><div class="col-xs-6"><p class="title-caleg">Calon</p><h5 class="content-caleg">'+namaCalon+'</h5><p class="title-caleg">Status Calon</p><h5 class="content-caleg">'+statusCalon+'</h5><p class="title-caleg">Gender Calon</p><h5 class="content-caleg">'+jkCalon+'</h5></div><div class="col-xs-6"><p class="title-caleg">Calon Wakil</p><h5 class="content-caleg">'+namaWakil+'</h5><p class="title-caleg">Status Calon</p><h5 class="content-caleg">'+statusCalon+'</h5><p class="title-caleg">Gender Wakil</p><h5 class="content-caleg">'+jkWakil+'</h5></div><div class="col-xs-12"><p class="title-caleg">Dukungan Partai</p><h5 class="content-caleg">'+toTitleCase(dukungan.toLowerCase())+'</h5></div></div><div class="detail"><div class="detail-menu"><ul><li><a href="#" class="info-caleg">Info Lengkap</a></li><li><a href="#" class="visi-caleg">Visi</a></li><li><a href="#" class="misi-caleg">Misi</a></li><li><a href="#" class="dukungan-caleg">Dukungan</a></li><li><a href="#" class="berkas-caleg">Berkas</a></li></ul></div>'+
						'<div class="detail-content">'+
							'<div class="info-caleg-content content-inner"><div class="row"><div class="col-xs-6"><p class="title-caleg">Pekerjaan Calon</p><h5 class="content-caleg">'+kerjaCalon+'</h5><p class="title-caleg">Usia Calon</p><h5 class="content-caleg">'+usiaCalon+'</h5><p class="title-caleg">Asal Calon</p><h5 class="content-caleg">'+daerahLahirCalon+'</h5><p class="title-caleg">Alamat Calon</p><h5 class="content-caleg">'+alamatCalon+'</h5></div><div class="col-xs-6"><p class="title-caleg">Pekerjaan Wakil</p><h5 class="content-caleg">'+kerjaWakil+'</h5><p class="title-caleg">Usia Wakil</p><h5 class="content-caleg">'+usiaWakil+'</h5><p class="title-caleg">Asal Wakil</p><h5 class="content-caleg">'+daerahLahirWakil+'</h5><p class="title-caleg">Alamat Wakil</p><h5 class="content-caleg">'+alamatWakil+'</h5></div></div>'+
							'</div>'+
							'<div class="berkas-caleg-content content-inner"><div class="row"><div class="col-xs-6"><p class="title-caleg">Kelengkapan Berkas</p><h5 class="content-caleg">'+berkas.kelengkapan+'</h5><p class="title-caleg">Syarat Dukungan</p><h5 class="content-caleg">'+berkas.pemenuhanSyaratDukungan+'</h5><p class="title-caleg">Status Dokumen Perbaikan</p><h5 class="content-caleg">'+berkas.penerimaanDokumenPerbaikan+'</h5></div><div class="col-xs-6"><p class="title-caleg">Status Penerimaan</p><h5 class="content-caleg">'+berkas.statusPenerimaan+'</h5></div></div>'+
							'</div>'+
							'<div class="dukungan-caleg-content content-inner"><div class="row"><div class="col-xs-6"><p class="title-caleg">Jenis Dukungan</p><h5 class="content-caleg">'+statistik.jenisDukungan+'</h5><p class="title-caleg">Jumlah Dukungan Awal</p><h5 class="content-caleg">'+statistik.jumlahDukunganAwal+'</h5><p class="title-caleg">Jumlah Dukungan Penetapan</p><h5 class="content-caleg">'+statistik.jumlahDukunganPenetapan+'</h5></div><div class="col-xs-6"><p class="title-caleg">Jumlah Dukungan Perbaikan</p><h5 class="content-caleg">'+statistik.jumlahDukunganPerbaikan+'</h5></div></div>'+
							'</div>'+
							'<div class="visi-caleg-content content-inner"></div>'+
							'<div class="misi-caleg-content content-inner"><div class="row"><div class="col-xs-6"></div></div></div>'+
							'</div>'+
						'</div></div>';// last closer div
				}

			if(isPrint){
				$('#right-content-card-caleg').append(strCardCaleg);
			}else{
				return strCardCaleg;
			}
	}

	$(document).on('click', '.info-caleg' , function(e) {
		$(this).parent().siblings().removeClass('active');
		$(this).parent().addClass('active');
		var idCandidate = $(this).closest('.card-caleg').attr('candidate-id');
		var allContentInner = $(this).closest('.detail').find('.detail-content').find('.content-inner');
		var contentInner = $(this).closest('.detail').find('.detail-content').children('.info-caleg-content.content-inner');
		if(contentInner.hasClass('show')){
			contentInner.slideUp(300, function(){
				contentInner.removeClass('show');
			});
		}else{
			contentInner.siblings().slideUp(300, function(){
				contentInner.siblings().removeClass('show');
				contentInner.slideDown(300, function(){
					contentInner.addClass('show');
				});
			});
		}

		e.preventDefault();
 	});

	$(document).on('click', '.berkas-caleg' , function(e) {
		$(this).parent().siblings().removeClass('active');
		$(this).parent().toggleClass('active');
		var idCandidate = $(this).closest('.card-caleg').attr('candidate-id');
		var allContentInner = $(this).closest('.detail').find('.detail-content').find('.content-inner');
		var contentInner = $(this).closest('.detail').find('.detail-content').children('.berkas-caleg-content.content-inner');
		
		if(contentInner.hasClass('show')){
			contentInner.slideUp(300, function(){
				contentInner.removeClass('show');
			});
		}else{
			contentInner.siblings().slideUp(300, function(){
				contentInner.siblings().removeClass('show');
				contentInner.slideDown(300, function(){
					contentInner.addClass('show');
				});
			});
		}

		e.preventDefault();
 	}); 

 	$(document).on('click', '.dukungan-caleg' , function(e) {
		$(this).parent().siblings().removeClass('active');
		$(this).parent().addClass('active');
		var idCandidate = $(this).closest('.card-caleg').attr('candidate-id');
		var allContentInner = $(this).closest('.detail').find('.detail-content').find('.content-inner');
		var contentInner = $(this).closest('.detail').find('.detail-content').children('.dukungan-caleg-content.content-inner');
		if(contentInner.hasClass('show')){
			contentInner.slideUp(300, function(){
				contentInner.removeClass('show');
			});
		}else{
			contentInner.siblings().slideUp(300, function(){
				contentInner.siblings().removeClass('show');
				contentInner.slideDown(300, function(){
					contentInner.addClass('show');
				});
			});
		}

		e.preventDefault();
 	});

 	$(document).on('click', '.visi-caleg' ,function(e){
 		$(this).parent().siblings().removeClass('active');
		$(this).parent().addClass('active');
		var idCandidate = $(this).closest('.card-caleg').attr('candidate-id');
		var contentInner = $(this).closest('.detail').find('.detail-content').children('.visi-caleg-content.content-inner');
		callVisimisi(key, idCandidate, function(data){
			console.log(data);
			visi = normalCaseTitle(data.data.results.vision_missions[0].visi);
			if(contentInner.hasClass('show')){
				contentInner.slideUp(300, function(){
					contentInner.empty();
					contentInner.removeClass('show');
				});
			}else{
				contentInner.empty();
				contentInner.append('<div class="row"><div class="col-xs-12"><p class="title-caleg">Visi</p><h5 class="content-caleg">'+visi+'</h5></div>');
				contentInner.siblings().slideUp(300, function(){
					contentInner.siblings().removeClass('show');
					contentInner.slideDown(300, function(){
						contentInner.addClass('show');
					});
				});
			}

		});

		e.preventDefault();
 	});

	function domListArray(uli){
		var size = uli.length;
		var str = '<ul style="padding-left: 0px; margin-top: 10px; ">';
		for (var i = 0; i <= size-1; i++) {
			if(((i%2) === 0) && (i !== 0)){
				str += '<li style="list-style-type: none; padding-top: 10px; padding-bottom: 10px; border-bottom: solid 1px #F1F1F1;">'+uli[i]+'</li>';
			}
		}
		str += '</ul>';
		return str;
	}

 	$(document).on('click', '.misi-caleg' ,function(e){
 		$(this).parent().siblings().removeClass('active');
		$(this).parent().addClass('active');
		var idCandidate = $(this).closest('.card-caleg').attr('candidate-id');
		var allContentInner = $(this).closest('.detail').find('.detail-content').find('.content-inner');
		var contentInner = $(this).closest('.detail').find('.detail-content').children('.misi-caleg-content.content-inner');
		callVisimisi(key, idCandidate, function(data){
			console.log(data);
			misi = normalCaseTitle(data.data.results.vision_missions[0].misi);
			listMisi = misi.split(/(\d+\))/g);
			listMisi = domListArray(listMisi);

			if(contentInner.hasClass('show')){
				contentInner.slideUp(300, function(){
					contentInner.empty();
					contentInner.removeClass('show');
				});
			}else{
				contentInner.empty();
				contentInner.append('<div class="row"><div class="col-xs-12"><p class="title-caleg">Misi</p>'+listMisi+'</div>');
				contentInner.siblings().slideUp(300, function(){
					contentInner.siblings().removeClass('show');
					contentInner.slideDown(300, function(){
						contentInner.addClass('show');
					});
				});
			}

		});

		e.preventDefault();
 	});

	//loadAllCaleg(key, 5, 0);

	function loadAllCaleg(key, limit, offset){
		//callCaleg(key, provinsi, region, limit, offset, function(data){
		//
		callCaleg(key, '', '', limit, offset, function(data){
			if(data.data.results.candidates.length > 0){
				return populateCaleg(data, 1);
			}else{
				alert('empty');
			}
		});
	}

	function loadCalegRegion(key, provinsi, region, limit, offset, isPrint, callback){
		callCaleg(key, provinsi, region, limit, offset, function(data){
			//console.log(data);
			if(data.data.results.candidates.length > 0){
				callback(populateCaleg(data, isPrint));
			}else{
				return callback(0);
			}
		});
	}

	function loadCalegProvinsi(key, provinsi, limit, offset, isPrint, callback){

		callCaleg(key, provinsi, null, limit, offset, function(data){
			//console.log(data);
			if(data.data.results.candidates.length > 0){
				return callback(populateCaleg(data, isPrint));
			}else{
				return callback(0);
			}
		});

	}

	var url = host;
	$(window).endlessScroll({	
		pagesToKeep: null,
		inflowPixels: 0,
        fireDelay: 0,
        fireOnce: false,
		loader: ' ',
		callback: function(i, p, d){

			if(d === 'next'){

				countCard = $('.card-caleg').length;
				console.log($('.select-provinsi').select2('val'));
				console.log($('.select-daerah').select2('val'));

				if(($('.select-daerah').select2('val') == 'all')&&($('.select-provinsi').select2('val')) == 'all'){
					loadAllCaleg(key, 5, countCard+5);					
				}else if($('.select-daerah').select2('val') == 'all'){
					loadCalegProvinsi(key, $('.select-provinsi').select2('val'), 5, countCard+5, 1);
				}else{
					loadCalegRegion(key, $('.select-provinsi').select2('val'), $('.select-daerah').select2('val'), 5, countCard+5, 1);
				}
				
			}
		},
		ceaseFireOnEmpty: false
	});
	
});

function callProvinsi(key, callback){
	$.ajax({
		type: 'GET',
		dataType: 'JSON',
		url: 'http://api.pemiluapi.org/calonpilkada/api/provinces?apiKey='+key,
		success: function(data){
			callback(data);
		}
	});
}

function callRegion(key, idProvinsi, callback){
	$.ajax({
		type: 'GET',
		dataType: 'JSON',
		url: 'http://api.pemiluapi.org/calonpilkada/api/regions?apiKey='+key+'&limit=269',
		success: function(data){
			callback(data, idProvinsi);
		}
	});	
}

function callCaleg(key, idProvinsi, idRegion, limit, offset, callback){
	if(limit || idProvinsi || idRegion){
		if(!idRegion){
			$.ajax({
				type: 'GET',
				dataType: 'JSON',
				url: 'http://api.pemiluapi.org/calonpilkada/api/candidates?apiKey='+key+'&provinsi='+idProvinsi+'&limit='+limit+'&offset='+offset,
				success: function(data){
					callback(data);
				}
			});	
		}else{
			$.ajax({
				type: 'GET',
				dataType: 'JSON',
				url: 'http://api.pemiluapi.org/calonpilkada/api/candidates?apiKey='+key+'&provinsi='+idProvinsi+'&daerah='+idRegion+'&limit='+limit+'&offset='+offset,
				success: function(data){
					callback(data);
				}
			});	
		}
	}else{
		$.ajax({
			type: 'GET',
			dataType: 'JSON',
			url: 'http://api.pemiluapi.org/calonpilkada/api/candidates?apiKey='+key,
			success: function(data){
				callback(data);
			}
		});	
	}
}

function callVisimisi(key, idCaleg, callback){

	$.ajax({
		type: 'GET',
		dataType: 'JSON',
		url: 'http://api.pemiluapi.org/calonpilkada/api/vision_missions?apiKey='+key+'&peserta='+idCaleg,
		success: function(data){
			callback(data);
			//localStorage.setItem('visimisi-'+idCaleg, JSON.stringify(data));
		}
	});

}