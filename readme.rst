###################
[DEPRECATED] PilkadaPintar
###################
[Perhatian: Repo ini sudah tidak aktif dikarenakan API Pemilu sudah ditutup sejak kompetisi berakhir]
PilkadaPintar adalah aplikasi berbasis web dengan bahasa Javascript yang berjalan diatas PHP yang berisikan informasi mengenai calon dan pasangan calon pada tiap-tiap daerah di seluruh Indonesia. PilkadaPintar bertujuan untuk memudahkan para pemilih untuk memelajari data para calon sebelum pilkada dilakukan. Aplikasi ini menggunakan PemiluiAPI sebagai sumber data.

*******************
Informasi Rilis
*******************

Versi kode pada repositori ini berada dalam pengembangan.

*******************
Persayaratan Server
*******************

PHP 5.4 atau yang terbaru

************
Instalasi
************

Salin dan tempel pada direktori utama server Anda.

*******
Lisensi
*******

The MIT License (MIT)

Copyright (c) 2015 Claudio Fresta Suharno

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.