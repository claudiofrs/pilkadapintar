
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" style="margin-top: 220px;">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          <h5 class="modal-title" id="mySmallModalLabel">Tentang Aplikasi </h5>
        </div>
        <div class="modal-body">
        <br>
          <center><img src="<?php echo base_url()?>/public/img/logo.png" alt="pilkadapintar"><p style="opacity: .4;">Versi 1.0</p></center>
          <hr>
          <p>Pilkadapintar.com merupakan aplikasi portal informasi dan pemantauan mengenai pasangan calon legislatif se-Indonesia pada gelaran Pilkada tahun 2015. </p>
          <hr style="margin-bottom: 10px;">
          <img src="<?php echo base_url();?>/public/img/apilogo.png" alt="apipemilu" width="50" style="opacity: .4;">
          <a href="http://twitter.com/claudiofrs" class="pull-right" style="margin-top: 10px;">@claudiofrs</a>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>

<script src="<?php echo base_url();?>/public/js/jquery-2.1.3.js"></script>
<script src="<?php echo base_url();?>/public/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>/public/js/jq.ss/jq.ss.js"></script>
<script src="<?php echo base_url();?>/public/js/jq.es/jq.es.js"></script>
<script src="<?php echo base_url();?>/public/js/jq.s2/s2.min.js"></script>
<script src="<?php echo base_url();?>/public/js/typeahead/th.js"></script>
<script src="<?php echo base_url();?>/public/js/script.js"></script>
<script type="text/javascript">
	//console.log = function(){}
</script>
