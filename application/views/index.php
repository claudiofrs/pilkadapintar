<?php include 'header.php'; ?>
<?php include 'navbar.php'; ?>

<hr>

<div class="row">
		
	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		<div class="thumbnail thumbnail-left">
			<img src="<?php echo base_url();?>/public/img/cover-lucya.png">
			<!-- <div class="cover-lucy"></div> -->
			<div class="statistik">
				<div class="row">
					<div class="col-xs-6">
						<h1>53%</h1>
						<p>RESPON POSITIF</p>
						<hr style="opacity: .7; margin-top: 10px; margin-bottom: 10px;">
						<p class="kalkulasi kalkulasi-sentimen" id="rasiyo">378 dari 1213 komentar adalah respon positif</p>
					</div>
					<div class="col-xs-6">
						<h1 class="value-pop-media" id="rasiyo">-</h1>
						<p>POPULARITAS MEDIA</p>
						<hr style="opacity: .7; margin-top: 10px; margin-bottom: 10px;">
						<p class="kalkulasi kalkulasi-media" id="rasiyo"></p>
					</div>
				</div>
			</div>
			<div class="detail">
				<div class="detail-menu">
					<ul>
						<li><a href="#" id="info-rasiyo">Info Calon</a></li>
						<li><a href="#" id="visi-rasiyo">Visi</a></li>
						<li><a href="#" id="misi-rasiyo">Misi</a></li>
						<li><a href="#" id="berita-rasiyo">Sepak Terjang</a></li>
					</ul>
				</div>
				<div class="detail-content">
					<div class="row content-inner" id="content-rasiyo"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		<div class="thumbnail thumbnail-right">
			<img src="<?php echo base_url();?>/public/img/cover-risma.png">
			<div class="statistik">
				<div class="row">
					<div class="col-xs-6">
						<h1>70%</h1>
						<p>RESPON POSITIF</p>
						<hr style="opacity: .7; margin-top: 10px; margin-bottom: 10px;">
						<p class="kalkulasi kalkulasi-sentimen" id="rasiyo">378 dari 1213 komentar adalah respon positif</p>
					</div>
					<div class="col-xs-6">
						<h1 class="value-pop-media" id="risma">-</h1>
						<p>POPULARITAS MEDIA</p>
						<hr style="opacity: .7; margin-top: 10px; margin-bottom: 10px;">
						<p class="kalkulasi kalkulasi-media" id="risma"></p>
					</div>
				</div>
			</div>
			<div class="detail">
				<div class="detail-menu">
					<ul>
						<li><a href="#" id="info-risma">Info Calon</a></li>
						<li><a href="#" id="visi-risma">Visi</a></li>
						<li><a href="#" id="misi-risma">Misi</a></li>
						<li><a href="#" id="berita-risma">Sepak Terjang</a></li>
					</ul>
				</div>
				<div class="detail-content">
					<div class="row content-inner" id="content-risma"></div>
				</div>
			</div>
		</div>
	</div>
</div>
	


<?php include 'footer.php'; ?>
<script src="<?php echo base_url();?>/public/js/sby.js"></script>
</body>
</html>