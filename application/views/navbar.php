<?php include 'header.php'; ?>
<body>
	
	<div class="container">

    <!-- NAVBAR -->
		<nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo base_url()?>"><img src="<?php echo base_url()?>/public/img/logo.png" alt="pilkadapintar"></a>
          </div>
          <div id="navbar">            
            <ul class="nav navbar-nav navbar-right">
              <li><a href="<?php echo base_url(); ?>">Pilkada Surabaya</span></a></li>
              <li><a href="<?php echo base_url(); ?>index.php/C_index/indonesia">Pilkada Indonesia</a></li>
              <li><a href="<?php echo base_url(); ?>index.php/C_index/stats">Peduli Demokrasi</a></li>
              <li><a href="<?php echo base_url(); ?>index.php/C_index/faq">Tanya Pilkada</a></li>
              <li><a href="#"><i class="fa fa-question-circle" data-toggle="modal" data-target=".bs-example-modal-sm"></i></a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
      <!-- EOF NAVBAR -->