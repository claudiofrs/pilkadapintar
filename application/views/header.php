<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Pilkadapintar</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>/public/css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/public/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/public/js/jq.s2/s2.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/public/css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
