<?php include 'header.php'; ?>
<?php include 'navbar.php'; ?>
<hr>

<div class="row">
	<div class="col-xs-4">
		<div class="outer-fixed" data-spy="affix" data-offset-top="30" data-offset-bottom="0" style="min-width: 236px; ">
			<div class="card card-filter">
					<h6 class="upper">Saring Data Statistik</h6><hr style="margin-top: 10px;margin-bottom: 0px;">
					<p class="select-title">Provinsi</p>
					<select class="select-provinsi-stats"></select>
					<!-- <p class="select-title">Data Statistik</p>
					<select class="select-data-stats"></select> -->
			</div>
		</div>
	</div>
	<div class="col-xs-8" id="right-content-stat"></div>
</div>



<?php include 'footer.php' ?>
<script src="<?php echo base_url();?>/public/js/stts.js"></script>
</body>
</html>