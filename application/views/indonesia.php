<?php include 'header.php'; ?>
<?php include 'navbar.php'; ?>
<hr>

<div class="row">
	<div class="col-xs-4">

		<div class="outer-fixed" data-spy="affix" data-offset-top="30" data-offset-bottom="0" style="min-width: 236px; ">
			<div class="card card-filter">
				<h6 class="upper">Saring Daftar Calon</h6><hr style="margin-top: 10px;margin-bottom: 0px;">
				<p class="select-title">Provinsi</p>
				<select class="select-provinsi"></select>
				<p class="select-title">Daerah</p>
				<select class="select-daerah"></select>
				<p class="select-title">Partai Pendukung</p>
				<select class="select-partai" disabled="disabled"><option value="all" selected="selected">Semua</option></select>
				<p class="select-title">Gender Calon</p>
				<select class="select-gender-calon" disabled="disabled"><option value="all" selected="selected">Semua</option></select>
				<center><br><br><img src="<?php echo base_url();?>/public/img/apilogo.png" alt="apipemilu" width="50" style="opacity: .4;"></center>
			</div>
		</div>

	</div>
	<div class="col-xs-8" id="right-content-card-caleg">

	</div>
</div>

<span class="arrow-totop">
	<a href="#top"><img src="<?php echo base_url()?>/public/img/top.png" alt=""></a>
</span>

<?php include 'footer.php'; ?>
<script src="<?php echo base_url();?>/public/js/ina.js"></script>
</body>
</html>