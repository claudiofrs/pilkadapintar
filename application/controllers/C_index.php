<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class C_index extends CI_Controller
{
	
	function index()
	{
		$this->load->view('index');
	}

	function indonesia()
	{
		$this->load->view('indonesia');
	}

	function stats(){
		$this->load->view('stats');
	}

	function faq(){
		$this->load->view('tanya');
	}
	
}


?>